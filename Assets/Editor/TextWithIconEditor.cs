﻿using System.Collections.Generic;
using UI.Sources;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using TextEditor = UnityEditor.UI.TextEditor;

[CustomEditor(typeof(TextWithIcon))]
public class TextWithIconEditor : TextEditor
{
    private SerializedProperty ImageScalingFactorProp;
    private SerializedProperty SpriteAtlasProp;

    protected override void OnEnable()
    {
        base.OnEnable();
        ImageScalingFactorProp = serializedObject.FindProperty("ImageScalingFactor");
        SpriteAtlasProp = serializedObject.FindProperty ("SpriteAtlas");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        EditorGUILayout.PropertyField(ImageScalingFactorProp, new GUIContent("Image Scaling Factor"));
        EditorGUILayout.PropertyField (SpriteAtlasProp, new GUIContent ("Sprite Atlas"));

        TextWithIcon myScript = (TextWithIcon)target;

        if (GUILayout.Button ("Build Object"))
        {
            myScript.BuildObject ();
        }


        serializedObject.ApplyModifiedProperties();
    
    }
}
