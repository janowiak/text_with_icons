﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

namespace UI.Sources
{
    public class TextWithIcon : Text
    {
        Image icon;
        Vector3 iconPosition;
        List<Image> icons;
        List<Vector3> positions = new List<Vector3> ();
        float _fontHeight;
        float _fontWidth;
        public float ImageScalingFactor;
        public SpriteAtlas SpriteAtlas;
        List<string> iconNames;
        string regex = @"\$";
        GameObject iconsContainer;

        string iconsRegex = @"abc";

        protected override void OnPopulateMesh(VertexHelper toFill)
        {
            base.OnPopulateMesh (toFill);
            List<UIVertex> vbo = new List<UIVertex> ();
            toFill.GetUIVertexStream (vbo);

            positions.Clear ();

            createIconsContainerIfNeeded ();

            var indexes = getIndexes (this);
            icons = GetComponentsInChildren<Image> ().ToList ();

            for (var y = 0; y < indexes.Count; y++)
            {
                var vector3s = new Vector3 [6];
                var startVertexIndex = indexes [y] * 6;
                var endVertexIndex = startVertexIndex + 6;

                var j = 0;
                for (var i = startVertexIndex; i < endVertexIndex; i++)
                {
                    vector3s [j] = vbo [i].position;
                    j++;
                    if (j == 6) j = 0;
                }

                positions.Add (getCenterOfVectors (vector3s));
                _fontHeight = Vector3.Distance (vector3s [0], vector3s [4]);
                _fontWidth = Vector3.Distance (vector3s [0], vector3s [1]);
            }

            //UpdateIconsPositions ();
        }

        void createIconsContainerIfNeeded ()
        {
            if (iconsContainer == null)
            {
                iconsContainer = new GameObject ();
                iconsContainer.name = "Icons Container";
                iconsContainer.transform.SetParent (this.gameObject.transform);
                RectTransform rectTransform = iconsContainer.AddComponent<RectTransform> ();
                rectTransform.offsetMin = new Vector2 (0, 0);
                rectTransform.offsetMax = new Vector2 (0, 0);
            }
        }

        private void Update()
        {
            //UpdateIconsPositions ();
        }

        public void BuildObject ()
        {
            
            createIconsContainerIfNeeded ();
            string text = this.text;
            Debug.Log (this.text);

            Regex regex = new Regex (@"\$");
            List<string> iconNames = new List<string> ();
            List<Transform> children = new List<Transform> ();

            for (int i = 0; i < iconsContainer.transform.childCount; i ++)
            {
                children.Add (iconsContainer.transform.GetChild (i));
            }

            foreach (Match match in regex.Matches (text))
            {
                Debug.Log ("!! " + match.Value.ToString ());
                iconNames.Add ("icon1");
            }

            for (int i = 0; i < iconNames.Count; i++)
            {
                bool found = false;
                for (int j = 0; j < children.Count; j ++)
                {
                    if (iconNames [i] == children [j].name)
                    {
                        found = true;
                        children [j].SetSiblingIndex (i);
                        children.RemoveAt (j);
                        break;
                    }
                }

                if (! found)
                {
                    GameObject newGameObject = new GameObject ();
                    newGameObject.name = iconNames [i];
                    Image image = newGameObject.AddComponent<Image> ();
                    image.sprite = SpriteAtlas.GetSprite (iconNames [i]);
                    image.preserveAspect = true;
                    newGameObject.transform.SetParent (iconsContainer.transform);
                    newGameObject.transform.SetSiblingIndex (i);
                }
            }

        }

        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange ();
            UpdateIconsPositions ();
        }

        void UpdateIconsPositions()
        {
            if (icons != null)
            {
                for (var i = 0; i < icons.Count; i++)
                {
                    icons [i].rectTransform.anchoredPosition = positions [i];
                    icons [i].rectTransform.sizeDelta = new Vector2 (_fontWidth * ImageScalingFactor, _fontHeight * ImageScalingFactor);
                }
            }
            
        }

        Vector3 getCenterOfVectors(Vector3 [] vectors)
        {
            Vector3 sum = Vector3.zero;

            if (vectors == null || vectors.Length == 0)
            {
                return sum;
            }

            for (int i = 0; i < vectors.Length; i++)
            {
                sum += vectors [i];
            }

            return sum / vectors.Length;
        }

        private List<int> getIndexes(Text textObject)
        {
            List<int> indexes = new List<int> ();

            foreach (Match match in Regex.Matches (textObject.text, regex))
            {
                indexes.Add (match.Index);
            }

            return indexes;
        }
    }
}
